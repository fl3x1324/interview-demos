package com.quickbase.devint;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * This DBManager implementation provides a connection to the database containing population data.
 * <p>
 * Created by ckeswani on 9/16/15.
 */
public class DBManagerImpl implements DBManager {

  private static final String FETCH_POPULATION_COUNT_GROUP_BY_COUNTRY =
      "SELECT C.CountryName as country, SUM(Ct.Population) as population\n" +
          "from Country C\n" +
          "         LEFT JOIN State S on C.CountryId = S.CountryId\n" +
          "         LEFT JOIN City Ct on S.StateId = Ct.StateId\n" +
          "GROUP BY country\n";

  public Connection getConnection() {
    Connection c = null;
    try {
      Class.forName("org.sqlite.JDBC");
      c = DriverManager.getConnection("jdbc:sqlite:resources/data/citystatecountry.db");
      System.out.println("Database file opened successfully");

    } catch (ClassNotFoundException cnf) {
      System.out.println("Loading driver class failed!");
    } catch (SQLException sqle) {
      System.out.println("sql exception:" + Arrays.toString(sqle.getStackTrace()));
    }
    return c;
  }

  public Map<String, Long> fetchLocalPopulationData(Connection c) {
    Map<String, Long> result = new HashMap<>();
    try {
      PreparedStatement stmt = c.prepareStatement(FETCH_POPULATION_COUNT_GROUP_BY_COUNTRY);
      stmt.execute();
      ResultSet resultSet = stmt.getResultSet();
      while (resultSet.next()) {
        result.put(resultSet.getString("country"), resultSet.getLong("population"));
      }
    } catch (SQLException se) {
      System.out
          .printf("An error occurred while executing statement! Message: %s%n", se.getMessage());
      se.printStackTrace();
    }
    return result;
  }
}
