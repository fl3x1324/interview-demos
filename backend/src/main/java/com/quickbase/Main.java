package com.quickbase;

import com.quickbase.devint.ConcreteStatService;
import com.quickbase.devint.DBManager;
import com.quickbase.devint.DBManagerImpl;
import com.quickbase.devint.IStatService;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 * The main method of the executable JAR generated from this repository. This is to let you execute
 * something from the command-line or IDE for the purposes of demonstration, but you can choose to
 * demonstrate in a different way (e.g. if you're using a framework)
 */
public class Main {

  public static void main(String[] args) {
    if ("test".equals(args[0])) {
      testAggregation();
      return;
    }

    System.out.println("Starting.");
    System.out.println("Getting DB Connection...");

    DBManager dbm = new DBManagerImpl();
    Connection c = dbm.getConnection();
    if (null == c) {
      System.out.println("Getting DB Connection failed! See above messages for details");
      System.exit(1);
    }
    Map<String, Long> localData = dbm.fetchLocalPopulationData(c);
    IStatService apiSvc = new ConcreteStatService();
    List<Pair<String, Integer>> externalData = apiSvc.GetCountryPopulations();
    Map<String, Long> result = aggregatePopulationData(localData, externalData);
    result.entrySet().stream()
        .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
        .forEach(e -> System.out.printf("%s: %d%n", e.getKey(), e.getValue()));
  }

  private static Map<String, Long> aggregatePopulationData(Map<String, Long> localData,
      List<Pair<String, Integer>> externalData) {
    Map<String, Long> result = new HashMap<>(localData);
    for (Pair<String, Integer> countryData : externalData) {
      result.putIfAbsent(countryData.getLeft(), Long.valueOf(countryData.getRight()));
    }
    return result;
  }

  private static void testAggregation() {
    Map<String, Long> mockLocalData = mockDBQuery();
    List<Pair<String, Integer>> mockExternalData = mockExternalCall();
    Map<String, Long> result = aggregatePopulationData(mockLocalData, mockExternalData);
    int expectedLength = 2;
    int actualLength = result.size();
    System.out.printf("Test aggregated data records count: Expected: %d, Actual: %d ... %s%n",
        expectedLength, actualLength, expectedLength == actualLength ? ":) PASS!" : ":( FAIL!");
    long distinctLength = result.keySet().stream().distinct().count();
    System.out
        .printf("Test aggregated data records are distinct: Expected: TRUE, Actual: %s ... %s%n",
            distinctLength == actualLength ? "YES" : "NO",
            distinctLength == actualLength ? ":) PASS!" : ":( FAIL!");
    boolean localValuePrefered = mockLocalData.get("SomeCountry1")
        .equals(result.get("SomeCountry1"));
    System.out.printf("Test aggregated data prefers local values ... %s%n",
        localValuePrefered ? ":) PASS!" : ":( FAIL!");
  }

  private static Map<String, Long> mockDBQuery() {
    return new HashMap<>(Collections.singletonMap("SomeCountry1", 9999L));
  }

  private static List<Pair<String, Integer>> mockExternalCall() {
    return Arrays.asList(new ImmutablePair<>("SomeCountry1", 9998),
        new ImmutablePair<>("SomeCountry2", 1212));
  }
}
